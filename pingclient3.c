#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>
#define BUFSIZE 1024
#define TIMEOUT_SECS 1


int main(int argc, char **argv) {
    int sockfd, n,serverlen;
    int portno = 1337;
    struct sockaddr_in serveraddr;
    struct hostent *server;
    char *hostname;
    int buf[BUFSIZE];
    struct timespec start, finish; 
    clock_gettime(CLOCK_REALTIME, &start); 

    if (argc != 2) {
       fprintf(stderr,"usage: %s <hostname>\n", argv[0]);
       return 1;
    }

    buf[0] = 0;
    hostname = argv[1];
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0){
        return 1;
    }

    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host as %s\n", hostname);
        exit(0);
    }

    while(1){

        bzero((char *) &serveraddr, sizeof(serveraddr));
        serveraddr.sin_family = AF_INET;
        bcopy((char *)server->h_addr, 
        (char *)&serveraddr.sin_addr.s_addr, server->h_length);
        serveraddr.sin_port = htons(portno);

        serverlen = sizeof(serveraddr);
        clock_gettime(CLOCK_REALTIME, &start); 
        buf[0] = buf[0] + 1;
        n = sendto(sockfd, buf, sizeof(buf), 0, &serveraddr, serverlen);

        if (n < 0){ 
            return 1;
        }
        
        struct timeval timeout;
        timeout.tv_sec = TIMEOUT_SECS;
        timeout.tv_usec = 0;
        fd_set read_fds;
        FD_ZERO(&read_fds);
        FD_SET(sockfd,&read_fds);

        n = select(sockfd+1, &read_fds,NULL,NULL,&timeout);
      
        clock_gettime(CLOCK_REALTIME, &finish); 
        float s = (finish.tv_sec - start.tv_sec); 

        if (n < 0){ 
            return 1;
        }
        else if(n == 0){
            printf("Packet %u: lost.\n",buf[0]);
        }
        else{
            char arr[BUFSIZE];
            n = recvfrom(sockfd, arr, sizeof(int), 0, &serveraddr, &serverlen);
            if(buf[0] != arr[0]){
                printf("Packet %u: wrong counter! Received %u instead of %u.\n", buf[0], arr[0],buf[0]);
            }
            else{
                printf("Packet %u: %f seconds.\n", buf[0], s);
            }
            sleep(1);
        }  
    }
}
