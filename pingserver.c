#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUFSIZE 1024


int main(int argc, char **argv) {
  int sockfd,clientlen; ; 
  int portno = 1337;
  struct sockaddr_in serveraddr; 
  struct sockaddr_in clientaddr; 
  struct hostent *hostp; 
  int buf[BUFSIZE]; 
  char *hostaddrp; 
  int optval; 
  int n; 


  if (argc > 1) {
    fprintf(stderr, "program takes no arguments.\n");
    return 1;
  }

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sockfd < 0) 
    return 1;


  bzero((char *) &serveraddr, sizeof(serveraddr));

  serveraddr.sin_family = AF_INET;
  serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
  serveraddr.sin_port = htons((unsigned short)portno);

  if (bind(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) 
    return 1;

  clientlen = sizeof(clientaddr);
  while (1) {

    bzero(buf, BUFSIZE);
    n = recvfrom(sockfd, buf, BUFSIZE, 0,
		 (struct sockaddr *) &clientaddr, &clientlen);
    if (n < 0)
      return 1;

   
    n = sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *) &clientaddr, clientlen);
    
    if (n < 0) 
      return 1;
     
  }
   
}
